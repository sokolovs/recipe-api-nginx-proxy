# Recipe App API Proxy

NGINX proxy for recipe api 

## Usage

### ENV Variables
* 'LISTEN_PORT' default :8000
* 'APP_HOST' hostname of the app to forward requests to (default: 'app')
* 'APP_PORT' ports of the app to forward requests to (default ':9000')
